package cn.com.vivebest;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(value="people",locations="classpath:people.properties")
//@PropertySource("classpath:people.properties")
public class ConfigurationPropertiesComponent {
	
	private String name;
	
	private int age;
	
	private String sex;
	
	private String phone;
	
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "name:"+name+",age="+age+",sex="+sex+",phone="+phone;
	}
	
	
	@Async
	public void async() throws Exception{
		System.out.println("--------------------->异步方法开始");
		TimeUnit.SECONDS.sleep(10);
		System.out.println("--------------------->异步方法结束");
	}
	
	@Async
	public Future<String> async2(){
		return new AsyncResult<String>("异步数据");
	}
}
