package cn.com.vivebest;

import java.time.LocalDateTime;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
//@EnableScheduling
public class ScheduledTask {
	
	@Scheduled(cron="* * * * * ?")
	public void printTime(){
		System.out.println("当前时间==="+LocalDateTime.now());
	}
	
	public String printName(){
		return "张三";
	}

}
