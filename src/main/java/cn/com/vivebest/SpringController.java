package cn.com.vivebest;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
//@Profile("dev")
public class SpringController {
	
	@Value("${server.port}")
	private int port;
	
	@Autowired
	private ConfigurationPropertiesComponent properties;
	
	@Autowired
	private ApplicationContext context;
	
	
	@GetMapping("/printPort")
	public int printPort(){
		return this.port;
	}
	
	@GetMapping("/getProperties")
	public String getProperties(){
		return properties.toString();
	}
	
	@GetMapping("/testThread")
	public void testThread(){
		List<Integer> list = Arrays.asList(1,2,3,4,5,6,7,8,9,10);
		SpringBootThread thread = context.getBean("springBootThread",SpringBootThread.class);
		thread.setList(list);
		new Thread(thread).start();
		new Thread(thread).start();
		new Thread(thread).start();
	}
	
	@GetMapping("/async")
	public void testAsync() throws Exception {
		properties.async();
		Future<String> future = properties.async2();
		System.out.println("future====="+future.get());
		System.out.println("---------------->�������");
	}
	
}
