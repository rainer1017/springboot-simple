package cn.com.vivebest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@ServletComponentScan
//@EnableAsync
public class SpringbootMain {
	
	@Value("${server.port}")
	private int port;
	 
	public static void main(String[] args) throws Exception {
		SpringApplication.run(SpringbootMain.class, args); 
	}
	
	@GetMapping("/")
	public int printPort(){
		return this.port;
	}

}
